readosm (1:1.1.0a+dfsg-3) unstable; urgency=medium

  * Bump Standards-Version to 4.7.0, no changes.
  * Bump debhelper compat to 13.
  * Use execute_{before,after} instead of override in rules file.
  * Enable Salsa CI.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 06 Mar 2025 20:22:30 +0100

readosm (1:1.1.0a+dfsg-2) unstable; urgency=medium

  * Bump watch file version to 4.
  * Update lintian overrides.
  * Bump Standards-Version to 4.6.1, no changes.
  * Update upstream metadata.
  * Bump debhelper compat to 12, changes:
    - Drop --list-missing from dh_install
  * Drop obsolete dh_strip override, dbgsym migration complete.
  * Add Rules-Requires-Root to control file.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 01 Dec 2022 18:43:00 +0100

readosm (1:1.1.0a+dfsg-1) unstable; urgency=medium

  * New upstream release.
    - Use epoch to make 1.1.0a+dfsg greater than 1.1.0+dfsg.
  * Bump Standards-Version to 4.5.0, no changes.
  * Add Build-Depends-Package field to symbols file.
  * Update gbp.conf to use --source-only-changes by default.
  * Drop Name field from upstream metadata.
  * Bump debhelper compat to 10, changes:
    - Drop --parallel option, enabled by default
    - Don't explicitly enable autoreconf, enabled by default
    - Drop dh-autoreconf build dependency
  * Update copyright fle.
  * Refresh patches.
  * Mark patches as Forwarded: not-needed.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 02 Aug 2020 17:10:14 +0200

readosm (1.1.0+dfsg-3) unstable; urgency=medium

  * Drop autopkgtest to test installability.
  * Add lintian override for testsuite-autopkgtest-missing.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 01 Aug 2018 18:52:10 +0200

readosm (1.1.0+dfsg-2) unstable; urgency=medium

  * Update copyright-format URL to use HTTPS.
  * Don't use libjs-jquery for Doxygen docs.
  * Bump Standards-Version to 4.1.5, no changes.
  * Update watch file to use HTTPS.
  * Update Vcs-* URLs for Salsa.
  * Drop obsolete get-orig-source target.
  * Strip trailing whitespace from control & rules files.
  * Drop obsolete dbg package.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 21 Jul 2018 14:01:09 +0200

readosm (1.1.0+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Drop Debian OpenStreetMap Team from Uploaders.
  * Change priority from extra to optional.
  * Bump Standards-Version to 4.1.0, changes: priority.
  * Update copyright years in copyright file.
  * Refresh patches.
  * Use pkg-info.mk variables instead of dpkg-parsechangelog output.
  * Add autopkgtest to test installability.
  * Update symbols for 1.1.0.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 25 Sep 2017 20:27:14 +0200

readosm (1.0.0e+dfsg-2) unstable; urgency=medium

  * Update Vcs-* URLs to use HTTPS.
  * Bump Standards-Version to 3.9.8, no changes.
  * Enable parallel builds.
  * Enable all hardening buildflags.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 01 May 2016 03:32:51 +0200

readosm (1.0.0e+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Update copyright years in copyright file.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 26 Jun 2015 20:22:46 +0200

readosm (1.0.0d-1) unstable; urgency=medium

  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 26 Apr 2015 20:29:38 +0200

readosm (1.0.0d-1~exp2) experimental; urgency=medium

  * Update Vcs-Browser URL to use cgit instead of gitweb.
  * Update my email to @debian.org address.
  * Add patch to disable HTML timestamps in Doxygen to allow
    reproducible builds.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 22 Feb 2015 19:23:43 +0100

readosm (1.0.0d-1~exp1) experimental; urgency=medium

  * New upstream release.
  * Update copyright file, update Autotools files.
  * Refresh patches.

 -- Bas Couwenberg <sebastic@xs4all.nl>  Fri, 19 Dec 2014 10:32:30 +0100

readosm (1.0.0c-1~exp1) experimental; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 3.9.6, no changes required.
  * Add upstream metadata.
  * Add gbp.conf to use pristine-tar by default.
  * Refresh patches.
  * Use Files-Excluded in copyright file to repack the upstream tarball.
  * Drop lintian override for no-upstream-changelog,
    shouldn't override pedantic tags.
  * Drop unused substitution variable from libreadosm1-dbg.
  * Remove .la & useless autogenerated doxygen files.

 -- Bas Couwenberg <sebastic@xs4all.nl>  Sun, 14 Dec 2014 20:43:39 +0100

readosm (1.0.0b+dfsg1-2) unstable; urgency=low

  * Add myself to Uploaders.
  * Use canonical URLs for Vcs-* fields.
  * Update copyright file.
  * Add dependency on libjs-query for doc package.
  * Use dh-autoreconf for retooling.
  * Update watch file, handle common mistakes.
  * Add Multi-Arch control fields.
  * Add lintian overrides for no upstream changelog, link to upstream timeline.
  * Add build dependency on graphiz, dot is used to generate documentation.
  * Move package maintenance from pkg-osm to pkg-grass.

 -- Bas Couwenberg <sebastic@xs4all.nl>  Wed, 02 Oct 2013 19:43:27 +0200

readosm (1.0.0b+dfsg1-1) unstable; urgency=low

  * New upstream version.
  * Move package from pkg-grass to pkg-osm

 -- David Paleino <dapal@debian.org>  Wed, 14 Nov 2012 17:00:32 +0100

readosm (1.0.0a+dfsg1-1) unstable; urgency=low

  * Initial release (Closes: #671678)

 -- David Paleino <dapal@debian.org>  Sun, 07 Oct 2012 17:24:29 +0200
